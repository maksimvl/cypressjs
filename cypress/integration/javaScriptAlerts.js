describe("JavaScript Alerts", () => {
    beforeEach(() => {
        cy.visit("/javascript_alerts");
    });

    it('should open JavaScript Alerts page', () =>  {
        cy.get("h3").should("have.text", "JavaScript Alerts")
    });

    it('Check alert text', () =>  {
        cy.checkPopUpWindow("Click for JS Alert","I am a JS Alert")
    });

    it('Accept confirm window', () =>  {
        cy.confirm("Click for JS Confirm","You clicked: Ok")
    });

    it('Input text in Promt and check', () =>  {
        cy.inputTextInPromt("123abc")
    });

});
