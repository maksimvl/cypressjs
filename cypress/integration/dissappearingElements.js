describe("Disappearing Elements", () => {
    beforeEach(() => {
        cy.visit("/disappearing_elements");
    });

    it('should open Disappearing Elements page', () =>  {
        cy.get("h3").should("have.text", "Disappearing Elements")
    });

    it('Back to Home', () =>  {
        cy.homeButton("Home","Available Examples")
    });

    it('About', () =>  {
        cy.aboutButton("About","Not Found")
    });

    it('Portfolio', () =>  {
        cy.portfolioButton("Portfolio","Not Found")
    });

});
