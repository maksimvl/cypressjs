Cypress.Commands.add('homeButton', (buttonName,statusText) => {
    cy.contains(buttonName).click()
    cy.visit("https://the-internet.herokuapp.com");
    cy.get("h2").should("have.text", statusText)
})
Cypress.Commands.add('checkPopUpWindow', (buttonName,text) => {
    cy.window().then((win) => {
        cy.stub(win, 'alert').as('alert')
        cy.contains(buttonName).click();
        cy.get('@alert').should('have.been.calledOnceWith', text)
    })
})

Cypress.Commands.add('confirm', (buttonName,statusText) => {
    cy.contains(buttonName).click()
    cy.on('window:confirm', () => true);
    cy.get('#result').should("have.text",statusText)
})

Cypress.Commands.add('inputTextInPromt', (text) => {
    cy.window().then(($win) => {
        cy.stub($win, 'prompt').returns(text)
        cy.contains('Click for JS Prompt').click()
    })
    cy.get('#result').should("have.text","You entered: 123abc")

})

Cypress.Commands.add('homeButton', (buttonName,statusText) => {
    cy.contains(buttonName).click()
    cy.visit("https://the-internet.herokuapp.com");
    cy.get("h2").should("have.text", statusText)
})

Cypress.Commands.add('aboutButton', (buttonName,statusText) => {
    cy.contains(buttonName).click()
    cy.visit("https://the-internet.herokuapp.com/about/", {failOnStatusCode: false});
    cy.get("h1").should("have.text", statusText)
})

Cypress.Commands.add('portfolioButton', (buttonName,statusText) => {
    cy.contains(buttonName).click()
    cy.visit("https://the-internet.herokuapp.com/portfolio/", {failOnStatusCode: false});
    cy.get("h1").should("have.text", statusText)
})
